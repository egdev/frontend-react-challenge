# Frontend React Challenge

## Instrucciones

Ejecute:

### `npm install`

### `npm start`

Ir a [http://localhost:3000](http://localhost:3000) en el navegador.

## Requerimientos:

-   Eliminar tarea de la lista
-   Marcar tarea como completada/no completada - Click en el botón a la izquierda de cada tarea (toggle)
-   Ordenar elementos según su prioridad (de high a low) - Click botón superior de la lista
-   Agregar color de borde a cada item de la lista según su prioridad (Verde para low y rojo para high)
-   Limpiar lista
