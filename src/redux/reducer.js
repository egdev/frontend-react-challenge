const initialState = {
    tasks: [
        {
            id: 0,
            name: 'Default Task',
            priority: 'Low',
            completed: true,
        },
    ],
}

export const addTask = (content) => ({
    type: 'ADD_TASK',
    payload: {
        content,
    },
})

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_TASK': {
            const tasks = [...state.tasks]
            const { content } = action.payload
            const newTaskId = tasks[tasks.length - 1]?.id + 1
            return {
                ...state,
                tasks: [...state.tasks, { id: newTaskId, ...content }],
            }
        }

        default:
            return state
    }
}
export default reducer
