import React from 'react'
import './ClearList.css'

const ClearList = ({ totalItems }) => {
    return (
        <div className="clear">
            <p className="total">Total tasks: {totalItems}</p>
            <button className="clear-button">Clear List</button>
        </div>
    )
}
export default ClearList
