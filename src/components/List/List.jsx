import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
    faTrash,
    faCheck,
    faMinus,
    faSortAmountUp,
} from '@fortawesome/free-solid-svg-icons'
import './List.css'

const List = ({ tasks }) => {
    return (
        <div className="list">
            {tasks.length > 1 && (
                <button className="order-button">
                    <FontAwesomeIcon icon={faSortAmountUp} />
                    High priority up
                </button>
            )}
            {tasks.map((item) => {
                return (
                    <div className="item" key={item.name}>
                        <p className="status">
                            <FontAwesomeIcon
                                icon={item.status ? faCheck : faMinus}
                            />
                        </p>
                        <p className="name">{item.name}</p>
                        <p className="priority">{item.priority}</p>
                        <button className="delete-button">
                            <FontAwesomeIcon icon={faTrash} />
                        </button>
                    </div>
                )
            })}
        </div>
    )
}
export default List
