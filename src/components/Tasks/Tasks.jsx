import React from 'react'
import { connect } from 'react-redux'
import NewTask from '../NewTask'
import List from '../List'
import ClearList from '../ClearList'
import { addTask } from '../../redux/reducer'
import './Tasks.css'

const Tasks = ({ addTask, tasks = [] }) => {
    return (
        <div className="container">
            <div className="title">TO-DO LIST</div>
            <NewTask addTask={addTask} />
            <List tasks={tasks} />
            <ClearList totalItems={tasks.length} />
        </div>
    )
}

const mapStateToProps = (state) => ({
    tasks: state.tasks,
})

const mapDispatch = (dispatch) => ({
    addTask: (content) => dispatch(addTask(content)),
})

export default connect(mapStateToProps, mapDispatch)(Tasks)
