import React, { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import './NewTask.css'

const NewTask = ({ addTask }) => {
    const priorities = ['Low', 'High']

    const [newTask, setNewTask] = useState('')
    const [currentPriority, setCurrentPriority] = useState(priorities[0])

    const onClickAddTask = (e) => {
        addTask({
            name: newTask,
            priority: currentPriority,
            status: false,
        })
        setNewTask('')
    }

    const changePriority = (priority) => {
        setCurrentPriority(priority)
    }

    return (
        <div>
            <input
                type="text"
                className="task-input"
                placeholder="New task"
                onChange={(e) => setNewTask(e.target.value)}
                value={newTask}
                maxLength="20"
            />
            <select
                className="priority-select"
                name="priority"
                onChange={(e) => changePriority(e.target.value)}
                value={currentPriority}
            >
                {priorities.map((priority) => (
                    <option key={priority} value={priority}>
                        {priority}
                    </option>
                ))}
            </select>
            <button
                className="add-button"
                type="button"
                onClick={(e) => onClickAddTask(e)}
                disabled={!newTask}
            >
                <FontAwesomeIcon icon={faPlus} />
            </button>
        </div>
    )
}
export default NewTask
